# Spring Boot Hazelcast
Hazelcast is a unified real-time data platform based on Java that combines a fast data store with stream processing. In a Hazelcast grid, data is evenly distributed among the nodes of a the cluster, allowing for horizontal scaling of processing and available storage. Backups are also distributed among nodes to protect against failure of any single node.

![Hazelcast Architecture](https://docs.hazelcast.com/imdg/3.12/_images/Embedded.png)

## Technologies Used
1. Spring Data JPA
2. Embedded H2 Database (can be replaced by MySQL)
3. Hazelcast and Mancenter
4. Lombok

## Important Maven dependencies used
```xml
<dependency>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-starter-cache</artifactId>
</dependency>
<dependency>
	<groupId>com.hazelcast</groupId>
	<artifactId>hazelcast</artifactId>
</dependency>
```

## Hazelcast Download Location
[https://hazelcast.com/open-source-projects/downloads/](https://hazelcast.com/open-source-projects/downloads/)

## Rest APIs
- GET request to http://localhost:8081/users to get all users
- GET request to http://localhost:8081/user/1 to get specific user
- POST request to http://localhost:8081/user to add a user
- PUT request to http://localhost:8081/user/1 to edit the user

```json
{
	"id" : 1,
	"firstName" : "John",
	"lastName" : "Doe",
	"emailId" : "john@domain.com",
	"age" : 35
}
```

### Build the Project skipping Tests
```mvn
mvn clean install -DskipTests
```

### Run the Project
```mvn
# start man-center
./hz-mc start

# start the project
java -jar spring-boot-hazelcast-0.0.1-SNAPSHOT.jar

# Management Center URL
http://localhost:8080

```
