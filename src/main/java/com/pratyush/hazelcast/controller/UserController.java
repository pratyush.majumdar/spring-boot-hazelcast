package com.pratyush.hazelcast.controller;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.pratyush.hazelcast.entity.User;
import com.pratyush.hazelcast.service.UserService;

@RestController
public class UserController {
	
	@Autowired
	UserService userService;
	
	@GetMapping(value = "/rest/users")
	public List<User> getAllUsers() {
		return userService.getAllUsers();
	}
	
	@GetMapping(value="/rest/user/{id}")
	public Optional<User> getUser(@PathVariable int id) {
		return userService.getUser(id);
	}
	
	@PostMapping(value = "/rest/user")
	public ResponseEntity<User> addUser(@RequestBody User user) {
		try {
			userService.addUser(user);
			return new ResponseEntity<User>(user, HttpStatus.CREATED);
		}catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PutMapping(value="/rest/user/{id}")
	public ResponseEntity<User> updateUser(@RequestBody User user, @PathVariable int id) {
		try {
			userService.updatUser(id, user);
			return new ResponseEntity<User>(user, HttpStatus.OK);
		}catch (NoSuchElementException e) {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
	}
}
