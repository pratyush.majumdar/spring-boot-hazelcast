package com.pratyush.hazelcast.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.pratyush.hazelcast.entity.User;

@Repository
public interface UserRepository extends CrudRepository<User, Integer> {

}
