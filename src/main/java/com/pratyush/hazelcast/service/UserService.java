package com.pratyush.hazelcast.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hazelcast.map.IMap;
import com.pratyush.hazelcast.entity.User;
import com.pratyush.hazelcast.repository.UserRepository;

@Service
public class UserService {

	@Autowired
	UserRepository userRepository;
	
	@Autowired
	private IMap<Integer, User> userMap;
	
	public List<User> getAllUsers() {
		List<User> users = new ArrayList<>();
		Iterator<User> iterator = userRepository.findAll().iterator();
		while(iterator.hasNext()) {
			users.add(iterator.next());
		}
		return users;
	}
	
	public Optional<User> getUser(int id) {
		Optional<User> user = null;
		if(userMap.get(id) != null) {
			user = Optional.ofNullable(userMap.get(id));
		} else {
			user = userRepository.findById(id);
		}
		return user;
	}
	
	public User addUser(User user) {
		userRepository.save(user);
		userMap.put(user.getId(), user);
		
		return user;
	}
	
	public Optional<User> updatUser(int id, User user) {
        Optional<User> existingUserOptional = userRepository.findById(id);
        if (existingUserOptional.isPresent()) {
        	User existingUser = existingUserOptional.get();
        	existingUser.setFirstName(user.getFirstName());
        	existingUser.setLastName(user.getLastName());
        	existingUser.setEmailId(user.getEmailId());
        	existingUser.setAge(user.getAge());
        	
        	userRepository.save(existingUser);
        	userMap.put(existingUser.getId(), existingUser);
        	
        	return Optional.ofNullable(existingUser);
        } else 
        	return null;
	}
}
