package com.pratyush.hazelcast;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.hazelcast.config.Config;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.map.IMap;
import com.pratyush.hazelcast.entity.User;

@Configuration
public class HazelcastConfiguration {
	@Bean
	public Config hazelCastConfig() {
		return new Config().setInstanceName("dev");
//				.setManagementCenterConfig(
//			new ManagementCenterConfig().setScriptingEnabled(true).addTrustedInterface("http://localhost:8080/"));
	}

	@Bean
	public HazelcastInstance hazelcastInstance(Config hazelCastConfig) {
		return Hazelcast.newHazelcastInstance(hazelCastConfig);
	}

	@Bean
	public IMap<Integer, User> userMap(HazelcastInstance hazelcastInstance) {
		return hazelcastInstance.getMap("userMap");
	}
}
